import os
import mysql.connector as mysql

print("[INFO] loaded hardware architecture model")

host = "10.0.0.21"
db = "encom"
msuser = "csh"
passw = "blahblah"

dbc = mysql.connect(host=host, database=db, user=msuser, password=passw)
if dbc is None:
    print("error, no dbc")

manufacturers = ["Atmel", "Microchip", "Cypress", "ARM", "Intel", "AMD"]
types = ["breadboard", "human solderable", "machine solderable"]
pins = [6, 8, 10, 12, 14, 16, 18, 10, 22, 24, 26, 28, 32, 34, 36, 38, 40]


class ValidHardware:
    def __init__(self, man, ty, pi):
        self.manufacturer = man
        self.type = ty
        self.pins = pi
        self.chip_identifier = ""

    def get_chip_pinout(self):
        return self.type, self.pins

    def check_valid_chip_conf(self):
        return

    def add_to_pack(self, file):
        return


def fetch_available_breadboard_chips():
    return
