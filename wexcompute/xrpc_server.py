# upload this to your rpc server

from xmlrpc.server import SimpleXMLRPCServer
import random
import threading as thr
import toml
from websvc import neo4j_handler as n4h

DEBUG = ("localhost", 7999)
PRODUCTION = ("10.0.0.21", 7999)
OFFLOADER = ("10.0.0.205", 7999)

OFFLOADER_FILE = ""
LOG_FILE = ""

linux = "/home/istria/stokproj/wexcompute/wconf/wex.toml"
macos = "/Users/vayentha/cs411-project-stocks/wexcompute/wconf/wex.toml"

with open(macos, "r") as conf:
    tt = toml.load(conf)
    print(tt)
    name = tt["owner"]["name"]
    tier = tt["owner"]["tier"]
    server_main = tt["servers"]["production"]["ip"]
    port_main = tt["servers"]["production"]["port"]
    staging = tt["servers"]["staging"]["ip"]
    staging_port = tt["servers"]["staging"]["port"]
    # print(name)
    # print(tier)
    # print(server_main)
    # print(port_main)
    # print(staging)
    # print(staging_port)
    # print(tt)


class VNode:
    def __init__(self, d=None):
        if d:
            self.data = d
        else:
            self.data = ""
        self.children = []
        self.links = []

    def add_data(self, data):
        self.data = data

    def add_children(self, c):
        if type(c) is VNode:
            self.children.append(c)
        else:
            return

    def add_link(self, l):
        if type(l) is VNode:
            self.links.append(l)
        else:
            return

    def autochild(self, d):
        self.children.append(VNode(d=d))

    def autolink(self, l):
        self.links.append(VNode(d=l))




def gen_hash(seed):
    return

def check_dict(item):
    if item["build"] is None:
        return "Error, item build not set."
    if item["cores"] is None:
        return "Error, num cores not set"


def is_even(n):
    return n % 2 == 0


# check this in rpc files
def test_rpc():
    return 10101


def get_net_info(ntype):
    if ntype == "main":
        return server_main, port_main
    if ntype == "staging":
        return staging, staging_port
    else:
        return "Error! type not recognized."


def rpc_build(item):
    if isinstance(item, dict):
        return item["build"]
    else:
        return 'error! this requires a valid dictionary(must be associative)'


def build_expanse_file(request):
    n = VNode(d=request)
    return n


def irpc_get_volume(l, w, h):
    return l * w * h


def irpc_getcontentframe(req):
    return


def remote_addnode(n, data):
    if type(n) is VNode:
        n.autochild(data)
    else:
        return

def rpc_cdn(content, key, ultronkey=None):
    if content == "movies":
        print("movie endpoint")
    if content == "music":
        print("music endpoint")
    if content == "tv":
        print("television endpoint")
    if content == "net-free":
        print("free network content endpoint")
    if content == "net-paid":
        print("paid network content endpoint")
    if content == "admin":
        if key is None or ultronkey is None:
            return
        else:
            print("admin netpoint")


def mode_x(request):
    if request == "rand":
        return random.randint(10000000, 99999999) + random.randint(10000000, 99999999)


if __name__ == "__main__":
    server = SimpleXMLRPCServer(DEBUG)
    print("listening on port 7999")
    server.register_function(is_even, "is_even")
    server.register_function(test_rpc, "test_rpc")
    server.register_function(rpc_build, "rpc_build")
    server.register_function(get_net_info, "get_net_info")
    server.register_function(mode_x, "mode_x")
    server.register_function(irpc_get_volume, "irpc_get_volume")
    server.register_function(rpc_cdn, "rpc_cdn")

    graphdb = n4h.VnetNeoHandler("bolt://10.0.0.21:7687")
    test_node = n4h.VnetMasterNode(title="Donghai").save()

    x = VNode("primary")
    x.autochild("hello")
    print(x.data)
    for item in x.children:
        print(item.data)
    server.serve_forever()