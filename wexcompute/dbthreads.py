import time
import threading
from queue import Queue

plock = threading.Lock()

class VThread(threading.Thread):
    def __init__(self, queue, args=(), kwargs=None):
        threading.Thread.__init__(self, args=(), kwargs=None)
        self.queue = queue
        self.daemon = True
        self.receive_messages = args[0]

    def run(self):
        print(threading.currentThread().getName(), self.receive_messages)
        val = self.queue.get()
        self.rrmsg(val)

    def rrmsg(self, message):
        if self.receive_messages:
            with plock:
                print(threading.currentThread().getName(), "Received {}".format(message))





if __name__ == '__main__':
    threads = []
    for t in range(10):
        q = Queue()
        threads.append(VThread(q, args=(t % 2 == 0,)))
        threads[t].start()
        time.sleep(0.1)

    for t in threads:
        t.queue.put("Print this!")

# test thread function
