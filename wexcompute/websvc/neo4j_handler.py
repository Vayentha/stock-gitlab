from neo4j import GraphDatabase
from neomodel import StructuredNode, StringProperty, RelationshipTo, RelationshipFrom, config
from neomodel import IntegerProperty

config.DATABASE_URL = 'bolt://:@10.0.0.21:7687'
DEBUG = True

# will put things into the neo4j database, but
# the important data structures and inserts are
# handled by neomodel
class VnetNeoHandler:

    def __init__(self, uri, user=None, password=None):
        self.driver = GraphDatabase.driver(uri)

    def close(self):
        self.driver.close()

    def greet(self, message):
        with self.driver.session() as session:
            greeting = session.write_transaction(self._create_and_return_greeting, message)
            print(greeting)

    @staticmethod
    def _create_and_return_greeting(tx, message):
        result = tx.run("CREATE (a:Greeting) "
                        "SET a.message = $message "
                        "RETURN a.message + ', from node ' + id(a)", message=message)
        return result.single()[0]

    @staticmethod
    def delete_db(tx):
        res = tx.run("MATCH (n) "
                     "DETACH DELETE n ")

    # DELETES THE ENTIRE DB BE CAREFUL
    def purge_database(self):
        if DEBUG:
            with self.driver.session() as session:
                purge = session.write_transaction(self.delete_db)
                print(purge)


class DataNode(StructuredNode):
    title = StringProperty(unique_index=True)
    author = RelationshipTo('Author' ,'AUTHOR')
    arbiter = RelationshipTo('Arbiter', 'ARBITER')
    mdn = RelationshipTo('MetaDataNode', 'CHILD')
    ds = RelationshipTo('DataStore', 'APP')

class VnetMasterNode(DataNode):
    title = StringProperty(unique_index=True)
    author = RelationshipTo('Author' ,'AUTHOR')
    arbiter = RelationshipTo('Arbiter', 'ARBITER')
    mdn = RelationshipTo('MetaDataNode', 'CHILD')
    ds = RelationshipTo('DataStore', 'APP')
    net = RelationshipTo('Vnet', 'SLAVE')


class Vnet(StructuredNode):
    title = StringProperty(unique_index=True)
    cmd_link = RelationshipTo('VnetMasterNode', 'MASTER')



class MetaDataNode(StructuredNode):
    title = StringProperty(unique_index=True)
    dn = RelationshipFrom('DataNode', 'PARENT')


class DataStore(StructuredNode):
    name = StringProperty(unique_index=False)
    dn = RelationshipFrom('DataNode', 'APP')
    host = StringProperty(unique_index=True)
    port = StringProperty(unique_index=True)


class Author(StructuredNode):
    name = StringProperty(unique_index=True)
    rank = IntegerProperty(index=True, default=1)
    dn = RelationshipFrom('DataNode', 'AUTHOR')
    ar = RelationshipFrom('Arbiter', 'ARBITER')


class Arbiter(StructuredNode):
    name = StringProperty(unique_index=True)
    au = RelationshipTo('Author', 'AUTHOR')
    dn = RelationshipFrom('Arbiter', 'ARBITER')

class BalancerNode(StructuredNode):
    name = StringProperty(unique_index=True)
    vm = RelationshipTo('DataNode', 'LOADBALANCER')

class Person(StructuredNode):
    name = StringProperty(unique_index=True)
    age = IntegerProperty(index=True, default=0)
    rank = IntegerProperty(index=True, default=1)




if __name__ == "__main__":
    g = VnetNeoHandler("bolt://10.0.0.21:7687")
    # g.greet("oy vey please work")
    test_node = VnetMasterNode(title="Rhododendron").save()
    store = DataStore(name="RDS-1", host="10.0.0.21", port=6543).save()
    test_md = MetaDataNode(title="UMDVN").save()
    arbiter = Arbiter(name="Mette").save()
    vayentha = Author(name="S.P. Sur", rank=12).save()
    vn = Vnet(title="vnet controller").save()
    test_node.author.connect(vayentha)
    test_node.arbiter.connect(arbiter)
    test_node.mdn.connect(test_md)
    test_node.ds.connect(store)
    test_node.net.connect(vn)
    vayentha.ar.connect(arbiter)
    # g.purge_database()
    g.close()