import socket
import sys

class webSVC:
    def __init__(self, ip, port, server):
        self.ip = ip
        self.port = port
        self.server = server
        self.socket2 = ""

    def create_svc(self):
        self.socket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server = (self.ip, self.port)
        self.socket2.bind(server)

    def start_svc(self):

        while True:
            # Wait for a connection
            print(sys.stderr, 'waiting for a connection')
            connection, client_address = self.socket2.accept()
            try:
                print(sys.stderr, 'connection from', client_address)

                # Receive the data in small chunks and retransmit it
                while True:
                    data = connection.recv(16)
                    print(sys.stderr, 'received "%s"' % data)
                    if data:
                        print(sys.stderr, 'sending data back to the client')
                        connection.sendall(data)
                    else:
                        print(sys.stderr, 'no more data from', client_address)
                        break

            finally:
                # Clean up the connection
                connection.close()


def manage_keys():
    return


def rebuild_cert():
    return