import graphene

class Services(graphene.Enum):
    READ = 1
    WRITE = 2
    SET = 3
    ADMIN = 4

class NetUser(graphene.Interface):
    id = graphene.ID()
    name = graphene.String()
    ipaddr = graphene.String()
    linkedusers = graphene.List(lambda: NetUser)
    svcs = graphene.List(Services)

    def resolve_linkedusers(self, info):
        return


class User(graphene.ObjectType):
    class Meta:
        interfaces = (NetUser,)
    Location = graphene.String()
