import psycopg2

# connection to postgres database
sindb = psycopg2.connect(
    user="sinestro1",
    password="",
    host="155.138.174.60",
    port="5432",
    database="sinestro1",
)

if sindb is None:
    print("error: cannot connect to sinestro")
else:
    print(sindb)

# get from 'apikeytable'
def pgapi_get_apikeytable(name):
    csr = sindb.cursor()
    query = f'''
    SELECT name, ip, current_key, date_added FROM apikeytable
    WHERE name = '{name}'
    '''
    csr.execute(query)
    res = csr.fetchall()
    return res

# get from 'bitfile' table
def pgapi_get_bitfiledb(name):
    csr = sindb.cursor()
    query = f'''
    SELECT filename, bitfile, signature FROM bitfiledb
    WHERE filename = '{name}'
    '''

# validates the api key from the database
def _validate_apikey(name, key):
    vals = pgapi_get_apikeytable(name)
    for val in vals:
        # print(val)
        nm = val[0]
        ky = val[2]
        print(nm)
        print(ky)
        if key == ky:
            # print('key validated!')
            return True
        else:
            # print("key failed validation!")
            return False








if __name__ == '__main__':
    print(pgapi_get_apikeytable("hm"))
    if _validate_apikey('hm', 'currkey'):
        print("key validated!")