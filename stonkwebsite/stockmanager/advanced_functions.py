import praw
import twitter
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import subprocess
import math

# Put your twitter API keys here!
consumer_key = ""
consumer_secret = ""
api_token = ""
secret_api_token = ""

sid = SentimentIntensityAnalyzer()

# fetch most recent official NASDAQ stock listings
def update_nasdaq_tickers():
    nasdaq_listings = subprocess.check_output("curl ftp://ftp.nasdaqtrader.com/SymbolDirectory/nasdaqlisted.txt", shell=True)
    open('nasdaq_listings.txt', 'wb').write(nasdaq_listings)
    return nasdaq_listings

# fetch senator stock trading history
def update_senator_trade_history():
    senator_trades = subprocess.check_output("curl https://senatestockwatcher.com/aggregate/all_ticker_transactions.json", shell=True)
    open('senate_trades.json', 'wb').write(senator_trades)
    return senator_trades

# fetch reddit comments
def update_wsb_comments():
    reddit = praw.Reddit(user_agent="", client_id="", client_secret="") # your reddit API keys go here!
    wsb = reddit.subreddit("wallstreetbets")
    comment_list = []

    for submission in wsb.hot(limit=50):
        submission.comments.replace_more(limit=10)
        for comment in submission.comments.list():
            comment_list.append(comment.body)

    return comment_list

# helper function for tf_idf
def get_ticker_number(tickers, ticker):
    for ticker_dict in tickers:
        if ticker in ticker_dict:
            return ticker_dict[ticker]
    return 0

# given a list of documents and desired stock tickers, calculates tf-idf
def calculate_tf_idf(documents, tickers):
    ticker_tfs = []

    for curr_doc in documents:
        curr_doc_dict = {}
        parsed_doc = curr_doc.split(' ')
        for word in parsed_doc:
            if any(word in ticker for ticker in tickers):
                if word not in curr_doc_dict:
                    curr_doc_dict[word] = 0
                curr_doc_dict[word] += 1
        ticker_tfs.append(curr_doc_dict)

    ticker_tf_idfs = []

    for doc_index, curr_doc in enumerate(documents):
        parsed_doc = curr_doc.split(' ')
        print(parsed_doc)
        tf_idf_doc = {}
        for word in parsed_doc:
            if any(word in ticker for ticker in tickers):
                word_tf_idf = float(ticker_tfs[doc_index][word] / (len(documents) + 1))
                word_tf_idf *= math.log(len(tickers) / (get_ticker_number(tickers, word) + 1))
                if word not in tf_idf_doc:
                    tf_idf_doc[word] = 0
                tf_idf_doc[word] = word_tf_idf
        ticker_tf_idfs.append(tf_idf_doc)

    return ticker_tf_idfs

# fetch most recent tweets tagged with the desired hashtag
def update_tweets(ticker):

    api = twitter.Api(consumer_key=consumer_key,
                    consumer_secret=consumer_secret,
                    access_token_key=api_token,
                    access_token_secret=secret_api_token)

    return api.GetSearch(raw_query=f"q=%23{ticker}&result_type=recent&since=2014-07-19&count=500")

# just fetch the tweet contents
def get_parsed_tweets(ticker):
    api_res = update_tweets(ticker)
    final_tweets = []
    for tweet in api_res:
        final_tweets.append(tweet[3])
    return final_tweets

# given words as input, classifies text as positive or negative
def classify_sentiment(text):
    return sid.polarity_scores(text)

# return % of documents which positively mention a ticker
def classify_percent_positive(documents, ticker):
    num_pos = 0
    num_doc_appeared_in = 0
    for document in documents:
        parsed_doc = document.split(' ')
        for word in parsed_doc:
            if ticker == word:
                doc_sentiment = classify_sentiment(document)
                if doc_sentiment['pos'] > doc_sentiment['neg']:
                    num_pos += 1
                num_doc_appeared_in += 1
                break
    if num_doc_appeared_in:
        return num_pos / num_doc_appeared_in
    return 0

# pass in list of documents i.e. twitter or reddit comments
# pass in list of tickers
# searches for mentions of tickers in documents to compute stock popularity
def calculate_ticker_frequency(documents, tickers):
    ticker_dict = {}
    for ticker in tickers:
        for document in documents:
            parsed_doc = document.split(' ')
            for word in parsed_doc:
                if word in ticker or ('#' + word) in ticker: # they might be using a hashtag
                    if word not in ticker_dict:
                        ticker_dict[word] = 0
                    ticker_dict[word] += 1
                    break

    return ticker_dict