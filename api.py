from wsgiref.simple_server import make_server
from pyramid.view import view_config
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.renderers import render_to_response
from pyramid.request import Request
from assets import realEstate as rs
from Crypto.Hash import SHA512
import mysql.connector as mysql
from stonkwebsite.stockmanager import serveStocks as ss
from OpenSSL import crypto
from OpenSSL.crypto import load_pkcs12, FILETYPE_PEM, FILETYPE_ASN1
import OpenSSL
import toml
import psycopg2

PRODUCTION = '10.0.0.21'
SECONDARY = '10.0.0.205'
PI = '192.168.86.215'
DEBUG = '127.0.0.1'



# certificate tests
p12 = crypto.load_pkcs12(open("wexcompute/websvc/private/proper.p12", "rb").read(), b"dong")
if p12 is None:
    print("error: pkcs12 cert could not be found.")
else:
    print(OpenSSL.crypto.dump_certificate(FILETYPE_PEM, p12.get_certificate()))

# mysql connection
dbc = mysql.connect(host=ss.host, database=ss.db, user=ss.msuser, password=ss.passw)
if dbc:
    print("mysql connected!")

'''
    This is the website API file. Pretty much every single API call will
    have its associated code here. 
'''

# config file checking here
with open("wexcompute/wconf/wex.toml", "r") as fil:
    cx = toml.load(fil)
    owner = cx["owner"]["name"]
    namespace = cx["owner"]["namespace"]
    print(owner)
    print(namespace)

# ============== API CALLS HERE ===============


# make this into a list of users from the database
def list_all_users(request):
    qst = request.path_qs
    parsed = qst.replace
    return Response('List of users\n')


# get an individual user from the database
def get_user(request):
    qst = request.path_qs   # get the actual query from the user
    parsed_qst = qst.replace('/users/', '')
    breakstr = parsed_qst.split('_')
    print(breakstr)   #this should have our firstname_lastname combo
    if ss.check_user_in_db_name(breakstr[0], breakstr[1]) == 1:
        return Response('A user named %(name1)s %(name2)s exists in the database\n' % request.matchdict)
    else:
        return Response('no user by the name of %(name1)s %(name2)s exists in the database.\n' %request.matchdict)

# webpack api call
def set_webpack(request):
    qst = request.path_qs
    parsed_qst = qst.replace('/webpack/', '')
    breakstr = parsed_qst.split("_")


# WORKS 10/24/2020
# deletes user from the database
def delete_user_from_db(userN):
    csr = dbc.cursor()
    query = f"""
    DELETE FROM users WHERE 
    username="{userN}"     
    """
    csr.execute(query)
    dbc.commit()
    return

# [DEPRECATED] serve real estate
def serve_real_estate(address, id=None):
    csr = dbc.cursor()
    query = f"""
    SELECT address, city, state, zip, price FROM
    realEstateProdHouse WHERE address LIKE "{address}"
    """
    csr.execute(query)
    dbres = csr.fetchall()
    print(f"api dbres: {dbres}")
    return dbres


# just an endpoint to the fetch/ subdirectory, does nothing right now
def api_user_stocks(request):
    qst = request.path_qs
    parsed = qst.replace("")
    return Response('user stocks endpoint\n')



# API call to get the user stocks
# looks up by USERNAME, not fname/lastname
def get_user_owned_stocks(request):
    qst = request.path_qs
    # print(qst)
    parsed_qst = qst.replace('/fetch/', '')
    # breakstr = parsed_qst.split('_')
    slist = ss.get_user_stocks(parsed_qst)
    for item in slist:
        for subitem in item:
            r = subitem
    return Response(f'{r}\n')


# [DEPRECATED] add stocks to db through api
def api_add_db_stocks(request):
    csr = dbc.cursor()
    query = f"""
    SELECT
    """
    return

# get stock amounts from API
# get stock amounts from API
def api_get_stock_amounts(request):
    qst = request.path_qs
    parsed_qst = qst.replace('/amounts/', '')
    # print(parsed_qst)
    current_uid = ss.get_userID_from_username(parsed_qst)
    # print(current_uid)
    for item in current_uid:
        for sub in item:
            actual_uid = sub
    print(actual_uid)
    owned = ss.build_test_stock_dict(actual_uid)
    owned = str(owned).replace('\'', '\"')
    return Response(f"{{ \"stonks\":   {owned}}}")


# test for checking if posting to the API works
@view_config(route_name='bigpost', renderer='json', request_method='POST')
def api_post_test(request):
    jobj = request.POST
    print(jobj)
    finaldict = []
    for item in jobj.items():
        finaldict.append(item)
    return {'bigyoungmoney': finaldict}

# [DEPRECATED] microservice calls
# @view_config(route_name='mcsv', renderer='json', request_method='POST')
def api_microservice_calls(request):
    qst = request.path_qs
    parsed_qst = qst.replace('/mcsv/', '')
    return

# Post API controls
# a simple post func
@view_config(route_name='creds', renderer='json', request_method='POST')
def post_creds(request):
    req2 = request.POST.get("uname")
    token = request.POST.get("token")
    secure_token = SHA512.new(data=token.encode('utf-8'))
    if req2 is None:
        return {'Error': 'nonetype set'}
    print(req2)
    print(token)
    print(secure_token.hexdigest())
    # getDBcreds(req2)
    return getDBcreds(req2)


def post_webpack(request):
    req1 = request.POST.get('webpack')
    req2 = request.POST.get('token')
    req3 = request.POST.get('net_info')


def get_test_real_estate(request):
    t = rs.getRealEstateTest()
    return Response(t)


def get_user_feed(request):
    feed_val = '{\"feed\": \"first feed val\"}'
    return Response(feed_val)


def getDBcreds(uname):
    csr = dbc.cursor()
    csr.execute(f"""SELECT credentials FROM users WHERE username = '{uname}'""")
    res = csr.fetchall()
    for item in res:
        for subitem in item:
            print(subitem)
            return subitem


def media_driver(request):
    qst = request.path_qs
    parsed_qst = qst.replace('/media/', '')
    print(parsed_qst)
    return Response(parsed_qst)


# real estate API hook v1
def api_re_fetch(request):
    qst = request.path_qs
    parsed_qst = qst.replace('/srs/' ,'')
    repl = parsed_qst.replace("_", " ")
    gg = serve_real_estate(address=str(repl))
    for item in gg:
        x = item
    return Response(f"{x}")


# to upload from a web page, we will need to attach to a form
def api_re_upload(request):
    return


# ======= END API CALLS =======


if __name__ == '__main__':
    config = Configurator()

    config.add_route('users', '/users')
    config.add_route('user', '/users/{name1}_{name2}')
    config.add_route('fetch', '/fetch')
    config.add_route('fetchlist', '/fetch/{name1}')
    config.add_route('add', '/add/{sname}')
    config.add_route('amounts', '/amounts/{sname}')
    config.add_route('bigpost', '/bigpost/')
    config.add_route('creds', '/creds/')
    config.add_route('realestatetest', '/retest/')
    config.add_route('userfeed', '/userfeed/' )
    config.add_route('media', '/media/{item}')
    config.add_route('srs', '/srs/{item}')



    config.add_view(list_all_users, route_name='users')
    config.add_view(get_user, route_name='user')
    config.add_view(api_user_stocks, route_name='fetch')
    config.add_view(get_user_owned_stocks, route_name='fetchlist')
    config.add_view(api_add_db_stocks, route_name='add')
    config.add_view(api_get_stock_amounts, route_name='amounts')
    config.add_view(api_post_test, route_name='bigpost')
    config.add_view(post_creds, route_name='creds')
    config.add_view(get_test_real_estate, route_name='realestatetest')
    config.add_view(get_user_feed, route_name='userfeed')
    config.add_view(media_driver, route_name='media')
    config.add_view(api_re_fetch, route_name='srs')

    # ---- run some simple tests here
    # delete_user_from_db("troggy")
    print("tests have completed.")
    # ------------------------------

    config.scan()
    app = config.make_wsgi_app()
    server = make_server(DEBUG, 6543, app)

    print(f"================SERVING NOW ON {DEBUG}=================")
    server.serve_forever()

# ===== EDIT LOG =====

# [6-20-2020 -- SAYAN] created API for fetching both users and stocks