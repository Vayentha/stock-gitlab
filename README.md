cs411-project-stocks


Commit code to this repository, there is no need to make a branch.

run "python3 manage.py runserver" to start development server, then navigate to 127.0.0.1/stocks to see the homepage. 

notes 6-15-2020:
- added config file `dbconfig.yaml` where sql login information can go. 
- added function to  `serveStocks.py` to update existing database information
