/**
    plan: make rust bindings that access opal kelly objects.
*/
extern crate libloading as lib;

fn call_dyn() -> Result<u32, Box<dyn std::error::Error>>
{
    let lib = lib::Library::new("/Users/vayentha/stock-gitlab/cerulean_fpga/src/_ok.so")?;
    let dyylib = lib::Library::new("/Users/vayentha/stock-gitlab/cerulean_fpga/src/libokFrontPanel.dylib")?;
    unsafe
        {
            let func: lib::Symbol<unsafe extern fn() -> u32> = lib.get(b"okCFrontPanel_CheckAPIVersion()")?;
            Ok(func())
        }
}

fn main()
{
    println!("Hello, world!");
    println!("{:?}",call_dyn());
}
