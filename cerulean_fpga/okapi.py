import ok
import toml
from os import path
import array
import ctypes
import psycopg2
from hashlib import sha3_512
import pymongo
import time
from threading import Thread, Event

from google.cloud import storage

dbc = psycopg2.connect(
    user="sinestro1",
    password="",
    host="155.138.174.60",
    port="5432",
    database="sinestro1",
)

myclient = pymongo.MongoClient("mongodb://10.0.0.21:27017/")
mydb = myclient["jvcom"]
mycol = mydb["jvcom"]




#/Users/vayentha/stock-gitlab/cerulean_fpga/key-05389001c47d.json


if myclient is None:
    print("ERROR: mongodb connection not made.")
else:
    print(myclient)


if dbc is None:
    print("error: database connection not found")
else:
    print(dbc)
device = ok.okCFrontPanel()

time.sleep(3)

# insert bitfile into postgres db
def insert_into_bitfile_db(file):
    csr = dbc.cursor()
    query = f"""
    INSERT INTO bitfiledb(filename, bitfile, signature) VALUES(%s, %s, %s)
    """
    if path.exists(f"bitfiles/{file}"):
        print("file exists!")
        fl = open(f"bitfiles/{file}", "rb").read()
        # print(fl)
        signature = sha3_512(fl)
        print(signature.hexdigest())
        csr.execute(query, (file, fl, signature.hexdigest()))
        dbc.commit()
    return


# allow only signed code to be executed
# in this case, allow only signed files to run
def verify_file_db(file):
    csr = dbc.cursor()
    query = f"""
    SELECT filename, bitfile, 
    """




deviceCount = device.GetDeviceCount()
# print(deviceCount)

print("-----------------------------------------------------------------------------------------------------------")
print(f"{ok.GetAPIVersionMajor()}.{ok.GetAPIVersionMinor()}.{ok.GetAPIVersionMicro()}")
for i in range(deviceCount):
    print("Device[{0}] Model: {1}".format(i, device.GetDeviceListModel(i)))
    print("Device[{0}] Serial: {1}".format(i, device.GetDeviceListSerial(i)))
print("-----------------------------------------------------------------------------------------------------------")

dev = ok.okCFrontPanel()
dev.OpenBySerial("1839000NN2")
# x7001 = ok.okCFrontPanel()
# x7001.OpenBySerial("2011000SWA")
dev.GetBoardModel()


def fpga_send_1024(data):
    return


def fpga_send_128(data):
    return

def WireOutFunc():

    dev.UpdateWireOuts()
    acc_x = dev.GetWireOutValue(0x20)
    acc_y = dev.GetWireOutValue(0x21)
    acc_z = dev.GetWireOutValue(0x22)
    mag_x = dev.GetWireOutValue(0x23)
    mag_y = dev.GetWireOutValue(0x24)
    mag_z = dev.GetWireOutValue(0x25)
    time.sleep(1)
    return [(acc_x, acc_y, acc_z), (mag_x, mag_y, mag_z)]


class MyThread(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event
        self.num_calls = 0
        self.current_vals = []
        self.additional_threadinfo = []

    def run(self):
        while not self.stopped.wait(1):
            self.current_vals = WireOutFunc()
            self.num_calls += 1

pipeline_arr = Event()
thr = MyThread(
    pipeline_arr
)  # thread to continuously fetch pipeline info every 15 seconds
thr.start()

# print(dev.GetDeviceID())
with open("conf/fpga.toml") as cfile:
    proper = toml.load(cfile)
    dev_model = proper["device"]["model"]
    dev_bitfile = proper["device"]["bitfile"]
    # print(dev_model)
    # print(dev_bitfile)
'''
if dev.GetBoardModel() == dev_model:
    print(f"valid FPGA found. Starting harness with file bitfiles/{dev_bitfile}")
    try:
        dev.ConfigureFPGA(
            f"/Users/vayentha/stock-gitlab/cerulean_fpga/bitfiles/{dev_bitfile}"
        )
    except Exception as e:
        print(e)
# device.OpenBySerial("1839000NN2")


# print(ok.okCFrontPanel_GetErrorString(-3))
print(x7001.GetWireInValue(0x00))
print(x7001.GetWireInValue(0x01))
x7001.SetWireInValue(0x00, 540)
x7001.SetWireInValue(0x01, 103)
x7001.UpdateWireIns()
print(x7001.GetWireInValue(0x00))
print(x7001.GetWireInValue(0x01))
dev.UpdateWireOuts()
'''
print("-----------------------------------------------------------------------------------------------------------")

dev.SetWireInValue(0x02, 0xAA)
dev.SetWireInValue(0x02, 0xAB)
dev.UpdateWireIns()

while(1):
    time.sleep(1)
    print(thr.current_vals)
    print(dev.GetWireInValue(0x02))

dev.Close()
print(
    "-----------------------------------------------------------------------------------------------------------"
)
