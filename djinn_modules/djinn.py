import os
import random
import toml

with open("conf/djinn.toml") as ff:
    proper = toml.load(ff)


class Djinn:
    def __init__(self):
        self.iso = ""
        self.jin = ""
        self.issue_num = random.randint(0, 99999)

    def set_iso(self, iso):
        self.iso = iso

    def set_jin(self, jin):
        self.jin = jin

