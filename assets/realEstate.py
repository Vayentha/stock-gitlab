import os

def getRealEstateTest():
    return "{\"userRealEstate\": \"you're broke as fuck\"}"

class GenericRE:
    def __init__(self, name, price):
        self.name = name
        self.price = price
        self.imageset = []
        self.json = self.toJSON()

    def toJSON(self):
        return (
        f'''
        {{ 
            "name": "{self.name}",
            "price": "{self.price}"
        }}
        ''')
