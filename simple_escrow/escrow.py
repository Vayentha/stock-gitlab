import os

class User:
    def __init__(self, fn, ln):
        self.firstname = fn
        self.ln = ln
        self.funds = 0

    def add_funds_wallet(self, f):
        self.funds = self.funds + f


class Purchase:
    poss_states = ["Created", "Locked", "Inactive", "Terminated"]
    def __init__(self, value, seller, buyer, moneyUsed):
        self.value = value
        self.seller = seller
        self.buyer = buyer
        self.escrow_state = "Created"
        self.fiat = moneyUsed
        self.money_field = ""   # send money to the escrow service for purchase
        self.insurance = ""

    def make_purchase(self):
        # remove user funds
        if type(self.buyer) is not User:
            print("Error, must be a user to get funds")
            return
        if self.buyer.funds < self.value:
            print("not enough funds in wallet")
            return
        self.buyer.funds = self.buyer.funds - self.value
        # send_money()
        return

    def confirm_purchase(self):
        return

    # checks if buyer has received item
    def buyer_recv_item(self):
        return

    def insure_payload(self, insurance):
        self.insurance = insurance
        return